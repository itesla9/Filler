/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 18:43:46 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 19:31:55 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

t_final		ft_make_fin3(t_final fin, t_a *tmp)
{
	if (fin.y < tmp->y)
	{
		fin.y = tmp->y;
		fin.x = tmp->x;
	}
	if (fin.y == tmp->y)
	{
		if (fin.x > tmp->x)
		{
			fin.y = tmp->y;
			fin.x = tmp->x;
		}
	}
	return (fin);
}

void		ft_first_part(t_final fin)
{
	ft_putnbr_fd(fin.y, 1);
	write(1, " ", 1);
	ft_putnbr_fd(fin.x, 1);
	write(1, "\n", 1);
}

void		ft_second_part(t_a *begin)
{
	ft_putnbr_fd(begin->y, 1);
	write(1, " ", 1);
	ft_putnbr_fd(begin->x, 1);
	write(1, "\n", 1);
}

void		ft_fill_coord(t_help box, t_struct *coord)
{
	int i;
	int x;
	int y;

	y = 0;
	i = 0;
	while (box.figur[y])
	{
		x = 0;
		while (box.figur[y][x])
		{
			if (box.figur[y][x] == '*' || box.figur[y][x] == '.')
			{
				coord[i].x = y;
				coord[i].n = x;
				i++;
			}
			x++;
		}
		y++;
	}
}

void		ft_coordinates(t_help box)
{
	t_struct	*coord;
	t_v		v;

	v.x = 0;
	v.y = 0;
	v.hits = 0;
	v.counter = 0;
	box.stars = box.frame[1].x * box.frame[1].n;
	coord = (t_struct *)malloc(sizeof(t_struct) * (box.stars));
	ft_fill_coord(box, coord);
	if (box.player == 1 && (box.frame[0].x == 15 || box.frame[0].x == 24))
		box.flag = 1;
	else if (box.player == 2 && box.frame[0].x == 100)
		box.flag = 1;
	else
		box.flag = 2;
	if (box.flag == 1)
		ft_victory(coord, box, v);
	else
		ft_victory2(coord, box, v);
}
