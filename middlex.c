/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b2.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 19:57:51 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 19:18:14 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		ft_find_roof_mid(t_help box)
{
	int x;
	int y;

	x = 0;
	y = 0;
	while (box.map[y])
	{
		while (box.map[y][x])
		{
			if (box.map[y][x] == 'X')
				break ;
			x++;
		}
		if (box.map[y][x] == 'X')
			break ;
		y++;
	}
	if (y == 0 || y == 1)
		return (1);
	return (0);
}

int		ft_find_floor_mid(t_help box)
{
	int x;
	int y;

	x = box.frame[0].n - 1;
	y = box.frame[0].x - 1;
	while (y >= 0)
	{
		while (box.map[y][x])
		{
			if (box.map[y][x] == 'X')
				break ;
			x--;
		}
		if (box.map[y][x] == 'X')
			break ;
		y--;
	}
	if (y == box.frame[0].n - 1)
		return (1);
	return (0);
}

void	ft_middlex_final(t_a *begin)
{
	t_a		*tmp;
	t_final fin;

	fin.y = begin->y;
	fin.x = begin->x;
	tmp = begin;
	while (tmp)
	{
		if (fin.y > tmp->y)
		{
			fin.y = tmp->y;
			fin.x = tmp->x;
		}
		tmp = tmp->next;
	}
	ft_first_part(fin);
}

void	ft_middlex_continue(t_a *begin, t_help box)
{
	t_a		*tmp;
	t_final fin;
	int		flag;

	flag = 0;
	fin.y = begin->y;
	fin.x = begin->x;
	tmp = begin;
	while (tmp && flag % 2 == 0)
	{
		fin = ft_make_fin3(fin, tmp);
		if (ft_find_floor_mid(box) == 1)
			flag++;
		tmp = tmp->next;
	}
	if (flag % 2 == 0)
		ft_first_part(fin);
	else
		ft_middlex_final(begin);
}

void	ft_middlex(t_a *begin, t_help box)
{
	int			flag;
	t_a			*tmp;
	t_final		fin;

	flag = 0;
	fin.y = begin->y;
	fin.x = begin->x;
	tmp = begin;
	while (tmp && flag % 2 == 0)
	{
		fin = ft_make_fin2(fin, tmp);
		if (ft_find_roof_mid(box) == 1)
			flag++;
		tmp = tmp->next;
	}
	if (flag % 2 == 0)
		ft_first_part(fin);
	else
		ft_middlex_continue(begin, box);
}
