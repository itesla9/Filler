/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strncat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 12:45:29 by yteslenk          #+#    #+#             */
/*   Updated: 2016/12/02 16:49:14 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_strncat(char *s1, const char *s2, size_t len)
{
	int		i;
	size_t	j;

	i = 0;
	j = 0;
	while (s1[i])
		i++;
	if (len > ft_strlen(s2))
	{
		while (s2[j])
		{
			s1[i] = s2[j];
			i++;
			j++;
		}
	}
	while (len-- > 0 && s2[j])
	{
		s1[i] = s2[j];
		i++;
		j++;
	}
	s1[i] = '\0';
	return (s1);
}
