/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strstr.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 12:57:53 by yteslenk          #+#    #+#             */
/*   Updated: 2016/11/26 14:09:52 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strstr(const char *p1, const char *p2)
{
	int			i;
	int			j;
	const char	*res;

	i = 0;
	j = 0;
	if (p2[j] == 0)
		return ((char*)p1);
	while (p1[i])
	{
		if (p1[i] == p2[j])
			res = &p1[i];
		j = 0;
		while (p1[i + j] == p2[j])
		{
			j++;
			if (p2[j] == '\0')
				return ((char*)res);
		}
		i++;
	}
	return (NULL);
}
