/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 13:16:37 by yteslenk          #+#    #+#             */
/*   Updated: 2016/12/02 17:18:41 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int			ft_words(const char *s, char c)
{
	int	i;
	int	size;

	i = 0;
	size = 0;
	while (s[i])
	{
		if (s[i] != c)
		{
			size++;
			while (s[i] != c && s[i] != '\0')
				i++;
		}
		else
			i++;
	}
	return (size);
}

static	char		*next_word(char const *s, char c)
{
	char		*word;
	size_t		size;

	size = 0;
	while (s[size] != c && s[size] != '\0')
		size++;
	word = ft_strsub(s, 0, size);
	return (word);
}

static	char		**ft_fillup(char **fresh, char const *s, char c)
{
	int	size;

	size = 0;
	while (*s)
	{
		if (*s != c)
		{
			fresh[size] = next_word(s, c);
			size++;
			while (*s != c && *s != '\0')
				s++;
		}
		else
			s++;
	}
	fresh[size] = NULL;
	return (fresh);
}

char				**ft_strsplit(char const *s, char c)
{
	char	**fresh;
	int		size;

	if (!s)
		return (NULL);
	size = ft_words(s, c);
	fresh = (char **)malloc(sizeof(char *) * (size + 1));
	if (!fresh)
		return (NULL);
	fresh = ft_fillup(fresh, s, c);
	return (fresh);
}
