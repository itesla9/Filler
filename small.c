/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b2.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 19:57:51 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 19:20:12 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		ft_find_o(t_help box)
{
	int x;
	int y;

	y = 0;
	while (box.map[y])
	{
		x = 0;
		while (box.map[y][x])
		{
			if (box.map[y][x] == 'O')
				break ;
			x++;
		}
		if (box.map[y][x] == 'O')
			break ;
		y++;
	}
	if ((y == 2 || y == 1) && x == 16)
		return (1);
	if (y == 0)
		return (1);
	return (0);
}

t_final	ft_make_fin(t_final fin, t_a *tmp)
{
	if (fin.y > tmp->y)
	{
		fin.y = tmp->y;
		fin.x = tmp->x;
	}
	if (fin.y == tmp->y)
	{
		if (fin.x < tmp->x)
		{
			fin.y = tmp->y;
			fin.x = tmp->x;
		}
	}
	return (fin);
}

void	ft_final_step_continue(t_a *begin)
{
	t_a			*tmp;
	t_final		fin;

	fin.y = begin->y;
	fin.x = begin->x;
	tmp = begin;
	while (tmp)
	{
		if (begin->y < tmp->y)
		{
			fin.y = tmp->y;
			fin.x = tmp->x;
		}
		if (begin->y == tmp->y)
		{
			if (begin->x < tmp->x)
			{
				fin.y = tmp->y;
				fin.x = tmp->x;
			}
		}
		tmp = tmp->next;
	}
	ft_first_part(fin);
}

void	ft_small(t_a *begin, t_help box, t_final fin)
{
	int			flag;
	t_a			*tmp;

	flag = 0;
	tmp = begin;
	while (tmp && flag % 2 == 0)
	{
		fin = ft_make_fin(fin, tmp);
		if (ft_find_o(box) == 1)
			flag++;
		tmp = tmp->next;
	}
	if (flag % 2 == 0)
		ft_first_part(fin);
	else
		ft_final_step_continue(begin);
}
