/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c2.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 21:36:14 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 18:29:36 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int			ft_find_x(t_help box)
{
	int x;
	int y;

	y = 0;
	while (box.map[y])
	{
		x = 0;
		while (box.map[y][x])
		{
			if (box.map[y][x] == 'X')
				break ;
			x++;
		}
		if (box.map[y][x] == 'X')
			break ;
		y++;
	}
	if (y == 1 || y == 2)
		return (1);
	return (0);
}

t_final		ft_make_fin2(t_final fin, t_a *tmp)
{
	if (fin.y > tmp->y)
	{
		fin.y = tmp->y;
		fin.x = tmp->x;
	}
	if (fin.y == tmp->y)
	{
		if (fin.x > tmp->x)
		{
			fin.y = tmp->y;
			fin.x = tmp->x;
		}
	}
	return (fin);
}

void		ft_smallx(t_a *begin, t_help box)
{
	int			flag;
	t_a			*tmp;
	t_final		fin;

	flag = 0;
	fin.y = begin->y;
	fin.x = begin->x;
	tmp = begin;
	while (tmp)
	{
		fin = ft_make_fin2(fin, tmp);
		if (ft_find_x(box) == 1)
			flag++;
		tmp = tmp->next;
	}
	if (flag % 2 == 0)
		ft_first_part(fin);
	else
		ft_second_part(begin);
}
