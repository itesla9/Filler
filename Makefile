#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/18 19:00:10 by yteslenk          #+#    #+#              #
#    Updated: 2017/02/18 19:45:13 by yteslenk         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = filler

CFLAGS = -Wall -Werror -Wextra

SRC =	a.c \
		b.c \
		big.c \
		bigx.c \
		c.c \
		check.c \
		get.c \
		filler.c \
		middle.c \
		middlex.c \
		small.c \
		smallx.c 

OBJ =	$(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	cd libft/ && $(MAKE)
	gcc $(CFLAGS) -o $(NAME) $(OBJ) libft/libft.a

%.o: %.c 
	gcc  $(CFLAGS) -c -o $@ $<

clean:
	rm -f $(OBJ)
	cd libft/ && $(MAKE) clean

fclean: clean
	rm -f $(NAME)
	cd libft/ && $(MAKE) fclean

re: fclean all

trash:
	rm *~	
