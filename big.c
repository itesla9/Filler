/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b2.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 19:57:51 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 19:19:43 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		ft_find_x_rev(t_help box)
{
	int x;
	int y;

	y = box.frame[0].x - 1;
	while (y > 0)
	{
		x = box.frame[0].n - 1;
		while (box.map[y][x] && x >= 0)
		{
			if (box.map[y][x] == 'X')
				break ;
			x--;
		}
		if (box.map[y][x] == 'X')
			break ;
		y--;
	}
	if (y == box.frame[0].x - 1)
		return (1);
	return (0);
}

int		ft_find_x_big(t_help box)
{
	int x;
	int y;

	y = 0;
	while (box.map[y])
	{
		x = 0;
		while (box.map[y][x])
		{
			if (box.map[y][x] == 'X')
				break ;
			x++;
		}
		if (box.map[y][x] == 'X')
			break ;
		y++;
	}
	if (y == 0 || y == 1)
		return (1);
	return (0);
}

void	ft_big(t_a *begin, t_help box, t_final fin)
{
	int			flag;
	t_a			*tmp;
	int			counter;

	flag = 0;
	counter = 0;
	tmp = begin;
	while (tmp)
	{
		fin = ft_make_fin(fin, tmp);
		if (ft_find_x_rev(box) == 1)
			counter = counter + 3;
		if (ft_find_x_big(box) == 1 && counter == 0)
			flag++;
		tmp = tmp->next;
	}
	if (flag % 2 == 0)
		ft_first_part(fin);
	else
		ft_final_step_continue(begin);
}
