/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 12:33:34 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 19:19:20 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HEADER_H
# define HEADER_H

# include <sys/types.h>
# include <stdio.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include "get_next_line.h"

typedef struct	s_struct
{
	int x;
	int n;
}				t_struct;

typedef struct	s_mass
{
	int			fd;
	int			player;
	int			m;
	int			f;
	int			flag;
	int			stars;
	char		**map;
	char		**figur;
	char		*line;
	t_struct	*frame;
}				t_help;

typedef struct	s_b
{
	int			y;
	int			x;
	struct s_b	*next;
}				t_a;

typedef struct	s_s
{
	int y;
	int x;
}				t_final;

typedef struct	s_c
{
	int y;
	int x;
	int hits;
	int counter;
}				t_v;

void			ft_putchar_fd(char c, int fd);
void			ft_putstr_fd(char const *s, int fd);
int				ft_atoi(const char *str);
void			ft_putnbr_fd(int n, int fd);
void			ft_fill_map(char **mass, char *first, int n, t_struct *frame);
void			ft_fill_figur(char **figur, char *first, int m,
								t_struct *frame);
void			ft_coordinates(t_help sex);
void			ft_victory(t_struct *coord, t_help sex, t_v v);
void			ft_victory2(t_struct *coord, t_help sex, t_v v);
void			ft_small(t_a *begin, t_help sex, t_final fin);
t_a				*ft_additional(t_a *tmp, t_v v);
t_a				*ft_make_begin(void);
void			ft_first_part(t_final fin);
void			ft_second_part(t_a *begin);
void			ft_smallx(t_a *begin, t_help sex);
void			ft_final_step_continue(t_a *begin);
void			ft_middle(t_a *begin, t_help sex);
void			ft_big(t_a *begin, t_help sex, t_final fin);
void			ft_bigx(t_a *begin, t_help sex);
t_final			ft_make_fin(t_final fin, t_a *tmp);
t_final			ft_make_fin2(t_final fin, t_a *tmp);
t_final			ft_make_fin3(t_final fin, t_a *tmp);
int				ft_find_roof(t_help sex);
int				ft_find_floor(t_help sex);
void			ft_middlex(t_a *begin, t_help sex);
int				ft_check1(t_v v, t_struct *coord, t_help sex);
int				ft_check2(t_v v, t_struct *coord, t_help sex);
#endif
