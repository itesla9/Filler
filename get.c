/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 19:08:56 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 18:37:37 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		ft_len(char *s)
{
	int i;

	i = 0;
	while (s[i])
		i++;
	return (i);
}

char	*ft_make_line(char *s, char letter)
{
	char	*buf;
	int		i;

	i = 0;
	if (s == 0)
	{
		buf = (char*)malloc(sizeof(char) * 2);
		buf[0] = letter;
		buf[1] = 0;
		return (buf);
	}
	else
	{
		buf = (char*)malloc(sizeof(char) * (ft_len(s) + 2));
		while (s[i])
		{
			buf[i] = s[i];
			i++;
		}
		buf[i] = letter;
		buf[++i] = 0;
	}
	free(s);
	return (buf);
}

char	*lines(int fd, char *s)
{
	char	letter;

	while (read(fd, &letter, 1) && letter != '\n')
	{
		if (!(s = ft_make_line(s, letter)))
			return (0);
	}
	return (s);
}

int		get_next_line(int fd, char **line)
{
	char	*s;

	s = NULL;
	if (line == NULL)
		return (-1);
	*line = NULL;
	if (!(s = lines(fd, s)))
		return (-1);
	*line = s;
	if (s == NULL)
		return (0);
	return (1);
}
