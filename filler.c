/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 18:40:50 by yteslenk          #+#    #+#             */
/*   Updated: 2017/05/15 13:55:22 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

t_help	ft_make_box(void)
{
	t_help box;

	box.frame = (t_struct *)malloc(sizeof(t_struct) * 2);
	box.m = 0;
	box.f = 0;
	return (box);
}

t_help	ft_figur(char *str, t_help box)
{
	int flag;

	flag = 0;
	while (*str)
	{
		if (*str >= '1' && *str <= '9' && flag == 0)
		{
			box.frame[1].x = ft_atoi(str);
			flag++;
			while (*str >= '1' && *str <= '9')
				str++;
		}
		if (*str >= '1' && *str <= '9' && flag > 0)
		{
			box.frame[1].n = ft_atoi(str);
			break ;
		}
		else
			str++;
	}
	box.figur = (char **)malloc(sizeof(char *) * (box.frame[1].x + 1));
	box.figur[box.frame[1].x] = 0;
	return (box);
}

t_help	ft_sizeofmap(char *str, t_help box)
{
	int flag;

	flag = 0;
	while (*str)
	{
		if (*str >= '1' && *str <= '9' && flag == 0)
		{
			box.frame[0].x = ft_atoi(str);
			flag++;
			while (*str >= '1' && *str <= '9')
				str++;
		}
		if (*str >= '1' && *str <= '9' && flag > 0)
		{
			box.frame[0].n = ft_atoi(str);
			break ;
		}
		else
			str++;
	}
	box.map = (char **)malloc(sizeof(char *) * (box.frame[0].x + 1));
	box.map[box.frame[0].x] = 0;
	return (box);
}

t_help	ft_add_main(t_help box)
{
	ft_coordinates(box);
	box.m = 0;
	box.f = 0;
	return (box);
}

int		main(void)
{
	t_help		box;
	char		*first;

	box = ft_make_box();
	while (get_next_line(0, &first) > 0)
	{
		if (first[0] == '$')
			box.player = ft_atoi(&first[10]);
		if (ft_strncmp("Plateau", first, 3) == 0)
			box = ft_sizeofmap(first, box);
		if (first[0] == '0')
		{
			box.map[box.m] = &first[4];
			(box.m)++;
		}
		if (ft_strncmp("Piece", first, 3) == 0)
			box = ft_figur(first, box);
		if (first[0] == '.' || first[0] == '*')
		{
			box.figur[box.f] = first;
			(box.f)++;
			if (box.f == box.frame[1].x)
				box = ft_add_main(box);
		}
	}
}
