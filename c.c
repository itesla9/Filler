/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 21:21:14 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 18:25:30 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	ft_dest2(t_help box, t_a *begin)
{
	if (box.frame[0].x == 100)
		ft_bigx(begin, box);
	if (box.frame[0].x == 24)
		ft_middlex(begin, box);
	if (box.frame[0].x == 15)
		ft_smallx(begin, box);
}

t_v		ft_make_hits(t_help box, t_v v, t_struct *coord)
{
	if (box.player == 2)
		v.hits = ft_check2(v, coord, box);
	else
		v.hits = ft_check1(v, coord, box);
	return (v);
}

void	ft_victory2(t_struct *coord, t_help box, t_v v)
{
	t_a *begin;
	t_a *tmp;

	begin = ft_make_begin();
	tmp = begin;
	v.y = box.frame[0].x - 1;
	while (v.y >= 0)
	{
		v.x = box.frame[0].n - 1;
		while (box.map[v.y][v.x])
		{
			v = ft_make_hits(box, v, coord);
			if (v.hits == box.stars + 1)
			{
				tmp = ft_additional(tmp, v);
				v.counter++;
			}
			v.x--;
		}
		v.y--;
	}
	tmp->next = NULL;
	ft_dest2(box, begin);
}
