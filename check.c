/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 16:36:02 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 18:22:41 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int			ft_check1(t_v v, t_struct *coord, t_help box)
{
	int i;
	int hits;

	i = -1;
	hits = 0;
	while (++i < box.stars)
	{
		if (coord[i].x + v.y >= box.frame[0].x ||
			coord[i].n + v.x >= box.frame[0].n)
			return (0);
		if (box.figur[coord[i].x][coord[i].n] == '.')
			hits++;
		else
		{
			if (coord[i].x + v.y < 0 || coord[i].n + v.x < 0)
				return (0);
			if (box.map[coord[i].x + v.y][coord[i].n + v.x] == 'X')
				return (0);
			if (box.map[coord[i].x + v.y][coord[i].n + v.x] == '.')
				hits++;
			if (box.map[coord[i].x + v.y][coord[i].n + v.x] == 'O')
				hits = hits + 2;
		}
	}
	return (hits);
}

int			ft_check2(t_v v, t_struct *coord, t_help box)
{
	int i;
	int hits;

	i = -1;
	hits = 0;
	while (++i < box.stars)
	{
		if (coord[i].x + v.y >= box.frame[0].x ||
			coord[i].n + v.x >= box.frame[0].n)
			return (0);
		if (box.figur[coord[i].x][coord[i].n] == '.')
			hits++;
		else
		{
			if (coord[i].x + v.y < 0 || coord[i].n + v.x < 0)
				return (0);
			if (box.map[coord[i].x + v.y][coord[i].n + v.x] == 'O')
				return (0);
			if (box.map[coord[i].x + v.y][coord[i].n + v.x] == '.')
				hits++;
			if (box.map[coord[i].x + v.y][coord[i].n + v.x] == 'X')
				hits = hits + 2;
		}
	}
	return (hits);
}
