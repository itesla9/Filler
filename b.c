/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b.c                                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/14 20:18:57 by yteslenk          #+#    #+#             */
/*   Updated: 2017/02/18 18:31:17 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

t_a			*ft_additional(t_a *tmp, t_v v)
{
	t_a *new;

	if (v.counter == 0)
	{
		tmp->y = v.y;
		tmp->x = v.x;
	}
	else
	{
		new = (t_a*)malloc(sizeof(t_a));
		new->y = v.y;
		new->x = v.x;
		tmp->next = new;
		tmp = tmp->next;
	}
	return (tmp);
}

t_a			*ft_make_begin(void)
{
	t_a *begin;

	begin = (t_a*)malloc(sizeof(t_a));
	begin->x = 0;
	begin->y = 0;
	begin->next = NULL;
	return (begin);
}

void		ft_dest(t_help box, t_a *begin)
{
	t_final fin;

	fin.y = begin->y;
	fin.x = begin->x;
	if (box.frame[0].x == 15)
		ft_small(begin, box, fin);
	if (box.frame[0].x == 24)
		ft_middle(begin, box);
	if (box.frame[0].x == 100)
		ft_big(begin, box, fin);
}

void		ft_victory(t_struct *coord, t_help box, t_v v)
{
	t_a *begin;
	t_a *tmp;

	begin = ft_make_begin();
	tmp = begin;
	while (box.map[v.y])
	{
		v.x = 0;
		while (box.map[v.y][v.x])
		{
			if (box.player == 2)
				v.hits = ft_check2(v, coord, box);
			else
				v.hits = ft_check1(v, coord, box);
			if (v.hits == box.stars + 1)
			{
				tmp = ft_additional(tmp, v);
				v.counter++;
			}
			v.x++;
		}
		v.y++;
	}
	tmp->next = NULL;
	ft_dest(box, begin);
}
